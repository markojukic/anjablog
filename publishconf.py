#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Anja Renko'
SITENAME = 'Anja Renko'
SITEURL = 'http://anjarenko.com'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'sl'

# My adjustments
THEME = 'source/anjasimple'
ARTICLE_URL = 'posts/{slug}'
ARTICLE_SAVE_AS = 'posts/{slug}/index.html'
TAG_URL = 'tags/{slug}'
TAG_SAVE_AS = 'tags/{slug}/index.html'
TAGS_URL= 'tags'
TAGS_SAVE_AS= 'tags/index.html'
CATEGORY_SAVE_AS = ''
CATEGORIES_SAVE_AS = ''
AUTHOR_SAVE_AS = ''
AUTHORS_SAVE_AS = ''
ARCHIVES_SAVE_AS = 'archives/index.html'
PAGE_URL = 'pages/{slug}'
PAGE_SAVE_AS = 'pages/{slug}/index.html'
LOCALE = 'sl_SI.UTF-8'
PLUGIN_PATHS = ["plugins"]
PLUGINS = ["neighbours"]

DISPLAY_PAGES_ON_MENU = False

FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

DEFAULT_PAGINATION = False
RELATIVE_URLS = False

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
