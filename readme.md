# Struktura bloga

	/anjarenko
		/public_html <-- Output
	
	/pelican
		/bin
		/lib
		/anjablog
			/output --> public_html
			/content --> ./Home/gordon/Dropbox/content
				/posts
				/pages
				/images
			/source [Git]
				/anjasimple
			/plugins
				neighbours.py
			publishconf.py [Git]
			pelicanconf.py	[Git]
			requirements.txt	[Git]
			
# Regeneracija

Po spremembi content vsebine na dropboxu je potrebno blog regenerirati.

	pelican /var/www/anjarenko/pelican/anjablog/content -s /var/www/anjarenko/pelican/anjablog/pelicanconf.py --ignore-cache

# Ureditev simlinkov

Symlink content na dropbox na serverju, zagnano pod userjem gordon:

	sudo ln -s /var/www/anjarenko/pelican/anjablog/content/ ~/Dropbox
	
Symlink output:

	sudo ln -s ~/var/www/anjarenko/pelican/anjablog/output/ /var/www/anjarenko /* needs checking */